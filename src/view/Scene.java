package view;

import java.awt.Graphics;

import javax.swing.JPanel;

import controller.KeyboardController;
import controller.PlatformController;
import model.gameElement.GameElement;
import model.gameElement.Mario;
import utils.CONST_RESOURCES;

@SuppressWarnings("serial")
public class Scene extends JPanel {

    private static final int MARIO_FREQUENCY = 2;
    private static final int MUSHROOM_FREQUENCY = 2;
    private static final int TURTLE_FREQUENCY = 2;
    private static final int MUSHROOM_DEAD_OFFSET_Y = 20;
    private static final int TURTLE_DEAD_OFFSET_Y = 30;
    private static final int FLAG_X_POS = 4650;
    private static final int CASTLE_X_POS = 4850;
    private static final int FLAG_Y_POS = 115;
    private static final int CASTLE_Y_POS = 145;

    private int background1PosX;
    private int background2PosX;
    private int mov;
    private int xPos;
    private int floorOffsetY;
    private int heightLimit;

    private PlatformController platformController;

    public Scene() {
        super();
        this.background1PosX = -50;
        this.background2PosX = 750;
        this.mov = 0;
        this.xPos = -1;
        this.floorOffsetY = 293;
        this.heightLimit = 0;

        platformController = PlatformController.getInstance();
        platformController.setScene(this);

        this.setFocusable(true);
        this.requestFocusInWindow();
        this.addKeyListener(new KeyboardController());
    }

    @Override
    public void paintComponent(Graphics g) {
        super.paintComponent(g);
        Graphics g2 = g;

        platformController.checkContacts();
        platformController.checkProximity();

        // Moving fixed model.objects
        this.updateBackgroundOnMovement();

        if (this.xPos >= 0 && this.xPos <= 4600) {
            platformController.moveAll();
        }

        g2.drawImage(platformController.getPlatform().imgBackground1(), this.background1PosX, 0, null);
        g2.drawImage(platformController.getPlatform().imgBackground2(), this.background2PosX, 0, null);
        g2.drawImage(platformController.getPlatform().castle(), 10 - this.xPos, 95, null);
        g2.drawImage(platformController.getPlatform().start(), 220 - this.xPos, 234, null);

        for (GameElement gameElement : platformController.getAllGameElements()) {
            g2.drawImage(gameElement.imgObj(), gameElement.x(), gameElement.y(), null);
        }

        g2.drawImage(platformController.getPlatform().imgFlag(), FLAG_X_POS - this.xPos, FLAG_Y_POS, null);
        g2.drawImage(platformController.getPlatform().imgCastle(), CASTLE_X_POS - this.xPos, CASTLE_Y_POS, null);

        if (platformController.getPlatform().mario().isJumping()) {
            g2.drawImage(platformController.getPlatform().mario().jump(),
                    platformController.getPlatform().mario().x(),
                    platformController.getPlatform().mario().y(), null);
        } else {
            g2.drawImage(platformController.getPlatform().mario().walk(CONST_RESOURCES.IMGP_CHARACTER_MARIO, MARIO_FREQUENCY),
                    platformController.getPlatform().mario().x(),
                    platformController.getPlatform().mario().y(), null);
        }

        if (platformController.getPlatform().mushroom().isAlive()) {
            g2.drawImage(platformController.getPlatform().mushroom().walk(CONST_RESOURCES.IMGP_CHARACTER_MUSHROOM, MUSHROOM_FREQUENCY),
                    platformController.getPlatform().mushroom().x(), platformController.getPlatform().mushroom().y(), null);
        } else {
            g2.drawImage(platformController.getPlatform().mushroom().deadImage(),
                    platformController.getPlatform().mushroom().x(),
                    platformController.getPlatform().mushroom().y() + MUSHROOM_DEAD_OFFSET_Y, null);
        }

        if (platformController.getPlatform().turtle().isAlive()) {
            g2.drawImage(platformController.getPlatform().turtle().walk(CONST_RESOURCES.IMGP_CHARACTER_TURTLE, TURTLE_FREQUENCY),
                    platformController.getPlatform().turtle().x(),
                    platformController.getPlatform().turtle().y(), null);
        } else {
            g2.drawImage(platformController.getPlatform().turtle().deadImage(),
                    platformController.getPlatform().turtle().x(),
                    platformController.getPlatform().turtle().y() + TURTLE_DEAD_OFFSET_Y, null);
        }
    }


    public int getFloorOffsetY() {
        return floorOffsetY;
    }

    public int getHeightLimit() {
        return heightLimit;
    }

    public int getMov() {
        return mov;
    }

    public int getxPos() {
        return xPos;
    }

    public void setBackground2PosX(int background2PosX) {
        this.background2PosX = background2PosX;
    }

    public void setFloorOffsetY(int floorOffsetY) {
        this.floorOffsetY = floorOffsetY;
    }

    public void setHeightLimit(int heightLimit) {
        this.heightLimit = heightLimit;
    }

    public void setxPos(int xPos) {
        this.xPos = xPos;
    }

    public void setMov(int mov) {
        this.mov = mov;
    }

    public void setBackground1PosX(int x) {
        this.background1PosX = x;
    }

    public void updateBackgroundOnMovement() {
        if (this.xPos >= 0 && this.xPos <= 4600) {
            this.xPos = this.xPos + this.mov;
            // Moving the screen to give the impression that Mario is walking
            this.background1PosX = this.background1PosX - this.mov;
            this.background2PosX = this.background2PosX - this.mov;
        }

        // Flipping between background1 and background2
        if (this.background1PosX == -800) {
            this.background1PosX = 800;
        } else if (this.background2PosX == -800) {
            this.background2PosX = 800;
        } else if (this.background1PosX == 800) {
            this.background1PosX = -800;
        } else if (this.background2PosX == 800) {
            this.background2PosX = -800;
        }
    }

    public Mario getMario() {
        return platformController.getPlatform().mario();
    }
}
