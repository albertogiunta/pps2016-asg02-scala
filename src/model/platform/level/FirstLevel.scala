package model.platform.level

import model.gameElement.ObjectFactory._
import model.gameElement._

import scala.collection.JavaConverters._
import scala.collection.mutable.ListBuffer

trait LevelStrategy {

     val mario: Mario
     val turtle: Turtle
     val mushroom: Mushroom
     var objects: ListBuffer[GameElement]
     var coins: ListBuffer[Coin]

     def disposeStillObjects: java.util.List[GameElement]

     def disposeCoins: java.util.List[Coin]
}

class FirstLevel() extends LevelStrategy {

     override val mario: Mario = newMario(300, 245)
     override val turtle: Turtle = newTurtle(950, 243)
     override val mushroom: Mushroom = newMushroom(800, 263)

     override var objects = new ListBuffer[GameElement]
     override var coins = new ListBuffer[Coin]

     insertObjectsIntoMap()

     private def insertObjectsIntoMap() = {
          objects append newBreakableBlock(400, 180)

          objects appendAll newInlineConfiguration(700, 155, 50, 0, 4, newBreakableBlock)

          objects append(newTunnel(600, 230),
               newTunnel(1000, 230),
               newBreakableBlock(1200, 180),
               newBlock(1270, 170),
               newBreakableBlock(1340, 160),
               newTunnel(1600, 230),
               newTunnel(1900, 230),
               newBlock(2000, 180),
               newTunnel(2500, 230),
               newBlock(2600, 160),
               newBlock(2650, 180),
               newTunnel(3000, 230),
               newBlock(3500, 160),
               newBlock(3550, 140),
               newTunnel(3800, 230),
               newTunnel(4500, 230))

          coins appendAll(newInlineConfiguration(400, 125, 100, -30, 4, newCoin) map(c => new Coin(c x, c y)))
          coins append(newCoin(1202, 140),
               newCoin(1272, 95),
               newCoin(1342, 40),
               newCoin(1650, 145),
               newCoin(2650, 145),
               newCoin(3000, 135),
               newCoin(3400, 125),
               newCoin(4200, 145),
               newCoin(4600, 40))
     }

     def disposeStillObjects: java.util.List[GameElement] = objects asJava

     def disposeCoins: java.util.List[Coin] = coins asJava


}