package model.platform

import java.awt.Image
import java.util

import model.gameElement.Coin
import model.gameElement.GameElement
import model.gameElement.Mario
import model.gameElement.Mushroom
import model.gameElement.Turtle
import model.platform.level.LevelStrategy
import utils.CONST_RESOURCES
import utils.ResourcesUtils

class Platform(val strategy: LevelStrategy) {
     var imgBackground1: Image = ResourcesUtils.getImage(CONST_RESOURCES.IMG_BACKGROUND)
     var imgBackground2: Image = ResourcesUtils.getImage(CONST_RESOURCES.IMG_BACKGROUND)
     var castle: Image = ResourcesUtils.getImage(CONST_RESOURCES.IMG_CASTLE)
     var start: Image = ResourcesUtils.getImage(CONST_RESOURCES.START_ICON)
     var imgCastle: Image = ResourcesUtils.getImage(CONST_RESOURCES.IMG_CASTLE_FINAL)
     var imgFlag: Image = ResourcesUtils.getImage(CONST_RESOURCES.IMG_FLAG)
     var mario: Mario = strategy.mario
     var turtle: Turtle = strategy.turtle
     var mushroom: Mushroom = strategy.mushroom
     var objects: java.util.List[GameElement] = strategy.disposeStillObjects
     var coins: util.List[Coin] = strategy.disposeCoins

     def removeObject(l: java.util.List[GameElement], i: Int): Unit = l.remove(i)
     def removeCoin(i: Int): Unit = coins.remove(i)
}