package model.gameElement

import scala.collection.mutable.ListBuffer

object ObjectFactory {
     def newBlock(x: Int, y: Int): Block = Block(x, y)

     def newBreakableBlock(x: Int, y: Int): BreakableBlock = BreakableBlock(x, y)

     def newTunnel(x: Int, y: Int): Tunnel = Tunnel(x, y)

     def newCoin(x: Int, y: Int): Coin = new Coin(x, y)

     def newMario(x: Int, y: Int): Mario = new Mario(x, y)

     def newTurtle(x: Int, y: Int): Turtle = new Turtle(x, y)

     def newMushroom(x: Int, y: Int): Mushroom = new Mushroom(x, y)

     def newInlineConfiguration(x: Int, y: Int, xStep: Int, yStep: Int, numberOfElements: Int, createObject: (Int, Int) => GameElement): ListBuffer[GameElement] = {
          var currentXStep: Int = 0
          var currentYStep: Int = 0
          val list: ListBuffer[GameElement] = new ListBuffer[GameElement]
          (0 to numberOfElements) foreach (_ => {
               list.append(createObject(x + currentXStep, y + currentYStep))
               currentXStep += xStep
               currentYStep += yStep
          })
          list
     }
}