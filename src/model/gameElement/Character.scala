package model.gameElement

import java.awt.Image

import controller.PlatformController
import utils.ElemProperties.{MARIO, MUSHROOM, TURTLE}
import utils.{CONST_RESOURCES, CollisionHelper, ResourcesUtils}
import view.Scene


trait Character extends GameElement {

     var counter: Int = 0

     var isAlive: Boolean = true

     var isMoving: Boolean = false

     var isToRight: Boolean = true

     def walk(name: String, frequency: Int): Image = {
          val str = CONST_RESOURCES.IMG_BASE + name + (if (!this.isMoving || {
               this.counter += 1
               this.counter
          } % frequency == 0) CONST_RESOURCES.IMGP_STATUS_ACTIVE
          else CONST_RESOURCES.IMGP_STATUS_NORMAL) + (if (this.isToRight) CONST_RESOURCES.IMGP_DIRECTION_DX
          else CONST_RESOURCES.IMGP_DIRECTION_SX) + CONST_RESOURCES.IMG_EXT
          ResourcesUtils.getImage(str)
     }
}

trait Enemy extends Character with Runnable {

     val Pause = 15

     var offsetX: Int = 1

     var deadImagePath: String

     var deadImage: Image = ResourcesUtils.getImage(deadImagePath)

}

object Mario {
     val MarioOffsetYInitial = 243
     val FloorOffsetYInitial = 293
     val JumpingLimit = 30
}

class Mario(override var x: Int,
            override var y: Int) extends Character {

     override var width: Int = MARIO.getWidth
     override var height: Int = MARIO.getHeight
     override var imgObj: Image = MARIO.getImageRes

     var scene: Scene = _
     var isJumping: Boolean = false
     var jumpingExtent: Int = 0
     var isDoubleJumping: Int = 0
     var currentLimit: Int = Mario.JumpingLimit

     def jump(): Image = {
          scene = PlatformController.getInstance.getScene
          var str: String = if (this.isToRight) CONST_RESOURCES.IMG_MARIO_SUPER_DX
          else CONST_RESOURCES.IMG_MARIO_SUPER_SX
          this.jumpingExtent += 1
          if (this.jumpingExtent < currentLimit && this.y > scene.getHeightLimit) this.y = this.y - 5
          else if (this.y + this.height < scene.getFloorOffsetY) this.y = this.y + 2
          else {
               str = if (this.isToRight) CONST_RESOURCES.IMG_MARIO_ACTIVE_DX
               else CONST_RESOURCES.IMG_MARIO_ACTIVE_SX
               this.isJumping = false
               this.isDoubleJumping = 0
               this.jumpingExtent = 0
               scene.setHeightLimit(0)
          }
          ResourcesUtils.getImage(str)
     }

     def incrementJumpOffset(): Unit = isDoubleJumping match {
          case 0 | 1 => isDoubleJumping += 1; currentLimit = Mario.JumpingLimit * isDoubleJumping; jumpingExtent = 1
          case _ =>
     }

     def contact(obj: GameElement): Unit = {
          scene = PlatformController.getInstance.getScene

          if (CollisionHelper.hitAhead(this, obj) && this.isToRight || CollisionHelper.hitBack(this, obj) && !this.isToRight) {
               scene.setMov(0)
               this.isMoving = false
          }

          if (CollisionHelper.hitBelow(this, obj) && this.isJumping) scene.setFloorOffsetY(obj.y)
          else if (!CollisionHelper.hitBelow(this, obj)) {
               scene.setFloorOffsetY(Mario.FloorOffsetYInitial)
               if (!this.isJumping) this.y = Mario.MarioOffsetYInitial
               if (CollisionHelper.hitAbove(this, obj)) scene.setHeightLimit(obj.y + obj.height) // the new sky goes below the object
               else if (!CollisionHelper.hitAbove(this, obj) && !this.isJumping) scene.setHeightLimit(0) // initial sky
          }
     }

     def contact(pers: Character): Unit = {

          if (CollisionHelper.hitAhead(this, pers) || CollisionHelper.hitBack(this, pers))
               if (pers.isAlive) {
                    this.isMoving = false
                    this.isAlive = false
               } else isAlive = true
          else if (CollisionHelper.hitBelow(this, pers)) {
               pers.isMoving = false
               pers.isAlive = false
          }
     }

     def contactBreakableBlock(objToContact: GameElement): Boolean = CollisionHelper.hitAbove(this, objToContact)

     def contactPiece(objToContact: GameElement): Boolean = CollisionHelper.hitBack(this, objToContact) ||
         CollisionHelper.hitAbove(this, objToContact) ||
         CollisionHelper.hitAhead(this, objToContact) ||
         CollisionHelper.hitBelow(this, objToContact)
}

abstract class AbstractEnemy(var x: Int,
                             var y: Int,
                             var width: Int,
                             var height: Int,
                             var imgObj: Image,
                             var deadImagePath: String) extends Enemy {

     def contact(obj: GameElement): Unit = {
          if (CollisionHelper.hitAhead(this, obj) && this.isToRight) {
               this.isToRight = false
               this.offsetX = -1
          }
          else if (CollisionHelper.hitBack(this, obj) && !this.isToRight) {
               this.isToRight = true
               this.offsetX = 1
          }
     }

     def run(): Unit = {
          while (true) {
               if (this.isAlive) {
                    this.move()
                    try Thread.sleep(Pause) catch {
                         case _: InterruptedException =>
                    }
               }
          }
     }

     override def move(): Unit = {
          this.x = this.x + (if (isToRight) 1 else -1)
     }
}

class Mushroom(x: Int, y: Int) extends AbstractEnemy(x, y, MUSHROOM.getWidth, MUSHROOM.getHeight, MUSHROOM.getImageRes, CONST_RESOURCES.IMG_MUSHROOM_DEAD_DX)

class Turtle(x: Int, y: Int) extends AbstractEnemy(x, y, TURTLE.getWidth, TURTLE.getHeight, TURTLE.getImageRes, CONST_RESOURCES.IMG_TURTLE_DEAD)