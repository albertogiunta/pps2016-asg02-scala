package model.gameElement

import java.awt.Image

import controller.PlatformController
import utils.ElemProperties.{BLOCK, COIN, TUNNEL, BREAKABLE_BLOCK}
import utils.{CONST_RESOURCES, ResourcesUtils}

trait GameElement {

     var x: Int

     var y: Int

     var width: Int

     var height: Int

     var imgObj: Image

     def move(): Unit = if (PlatformController.getInstance.getScene.getxPos >= 0) this.x = this.x - PlatformController.getInstance.getScene.getMov

}

abstract class AnimatedGameObject(var x: Int,
                                  var y: Int,
                                  var width: Int,
                                  var height: Int,
                                  var imgObj: Image) extends GameElement with Runnable {

     val Pause = 10

     def setImageOnMovement(): Image

     override def run(): Unit = {
          while (true) {
               this.setImageOnMovement()
               try Thread.sleep(Pause)
               catch {
                    case e: InterruptedException =>
               }
          }
     }
}

case class Block(override var x: Int,
                 override var y: Int) extends GameElement {

     override var width: Int = BLOCK.getWidth
     override var height: Int = BLOCK.getHeight
     override var imgObj: Image = BLOCK.getImageRes

}

case class BreakableBlock(override var x: Int,
                          override var y: Int) extends GameElement {

     override var width: Int = BREAKABLE_BLOCK.getWidth
     override var height: Int = BREAKABLE_BLOCK.getHeight
     override var imgObj: Image = BREAKABLE_BLOCK.getImageRes

}

case class Tunnel(override var x: Int,
                  override var y: Int) extends GameElement {

     override var width: Int = TUNNEL.getWidth
     override var height: Int = TUNNEL.getHeight
     override var imgObj: Image = TUNNEL.getImageRes

}

class Coin(x: Int,
           y: Int) extends AnimatedGameObject(x, y, COIN.getWidth, COIN.getHeight, COIN.getImageRes) {

     val FlipFrequency = 10
     private var _counter = 0

     override def setImageOnMovement(): Image = {
          this._counter += 1
          ResourcesUtils.getImage(if (this._counter % FlipFrequency == 0) CONST_RESOURCES.IMG_PIECE1
          else CONST_RESOURCES.IMG_PIECE2)
     }

}
