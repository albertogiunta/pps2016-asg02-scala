package utils;

import model.gameElement.Character;
import model.gameElement.GameElement;

public class CollisionHelper {

    public static final int PROXIMITY_MARGIN = 10;
    public static final int BOH_MARGIN       = 5;

    public static boolean hitAhead(GameElement hitterObj, GameElement hitObj) {
        if (hitterObj.x() + hitterObj.width() < hitObj.x() || hitterObj.x() + hitterObj.width() > hitObj.x() + 5 ||
                hitterObj.y() + hitterObj.height() <= hitObj.y() || hitterObj.y() >= hitObj.y() + hitObj.height()) {
            return false;
        } else {
            return true;
        }
    }

    public static boolean hitAhead(Character hitterObj, GameElement hitObj) {
        return hitterObj.isToRight() && hitAhead(((GameElement) hitterObj), hitObj);
    }

    public static boolean hitBack(GameElement hitterObj, GameElement hitObj) {
        return !(hitterObj.x() > hitObj.x() + hitObj.width() || hitterObj.x() + hitterObj.width() < hitObj.x() + hitObj.width() - 5 ||
                hitterObj.y() + hitterObj.height() <= hitObj.y() || hitterObj.y() >= hitObj.y() + hitObj.height());
    }

    public static boolean hitBelow(GameElement hitterObj, GameElement hitObj) {
        int offset = hitObj instanceof Character ? BOH_MARGIN : 0;
        if (hitterObj.x() + hitterObj.width() < hitObj.x() + offset || hitterObj.x() > hitObj.x() + hitObj.width() - 5 ||
                hitterObj.y() + hitterObj.height() < hitObj.y() || hitterObj.y() + hitterObj.height() > hitObj.y() + 5) {
            return false;
        } else
            return true;
    }

    public static boolean hitAbove(GameElement hitterObj, GameElement hitObj) {
        if (hitterObj.x() + hitterObj.width() < hitObj.x() + 5 || hitterObj.x() > hitObj.x() + hitObj.width() - 5 ||
                hitterObj.y() < hitObj.y() + hitObj.height() || hitterObj.y() > hitObj.y() + hitObj.height() + 5) {
            return false;
        } else {
            return true;
        }
    }

    public static boolean isNearby(GameElement hitterObj, GameElement hitObj) {
        if ((hitterObj.x() > hitObj.x() - PROXIMITY_MARGIN && hitterObj.x() < hitObj.x() + hitObj.width() + PROXIMITY_MARGIN) ||
                (hitterObj.x() + hitterObj.width() > hitObj.x() - PROXIMITY_MARGIN && hitterObj.x() + hitterObj.width() < hitObj.x() + hitObj.width() + PROXIMITY_MARGIN))
            return true;
        return false;
    }
}
