package utils;

import javax.swing.*;
import java.awt.*;
import java.net.URL;

public class ResourcesUtils {
    public static URL getResource(String path){
        return ResourcesUtils.class.getClass().getResource(path);
    }

    public static Image getImage(String path){
        return new ImageIcon(getResource(path)).getImage();
    }
}
