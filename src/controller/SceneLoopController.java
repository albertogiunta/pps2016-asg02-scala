package controller;

public class SceneLoopController implements Runnable {

    private final int pause = 3;

    public void run() {
        while (true) {
            PlatformController.getInstance().getScene().repaint();
            try {
                Thread.sleep(pause);
            } catch (InterruptedException e) {
            }
        }
    }
} 
