package controller;

import java.util.LinkedList;
import java.util.List;

import model.gameElement.BreakableBlock;
import model.gameElement.Coin;
import model.gameElement.GameElement;
import model.platform.Platform;
import model.platform.level.FirstLevel;
import utils.CONST_RESOURCES;
import utils.CollisionHelper;
import view.Scene;

public class PlatformController {

    private static PlatformController instance;
    private        Platform           platform;
    private        Scene              scene;

    public static PlatformController getInstance() {
        if (instance == null) {
            instance = new PlatformController();
        }
        return instance;
    }

    private PlatformController() {
        platform = new Platform(new FirstLevel());
    }

    public void checkContacts() {

        for (GameElement stillObject : platform.objects()) {
            if (CollisionHelper.isNearby(platform.mario(), stillObject)) {
                platform.mario().contact(stillObject);
                if (stillObject instanceof BreakableBlock && platform.mario().contactBreakableBlock(stillObject)) {
                    platform.removeObject(platform.objects(), platform.objects().indexOf(stillObject));
                }
            }

            if (CollisionHelper.isNearby(platform.mushroom(), stillObject)) {
                platform.mushroom().contact(stillObject);
            }

            if (CollisionHelper.isNearby(platform.turtle(), stillObject)) {
                platform.turtle().contact(stillObject);
            }
        }

        for (Coin coin : platform.coins()) {
            if (platform.mario().contactPiece(coin)) {
                AudioController.playSound(CONST_RESOURCES.AUDIO_MONEY);
                platform.removeCoin(platform.coins().indexOf(coin));
            }
        }
    }

    public void checkProximity() {
        if (CollisionHelper.isNearby(platform.mario(), platform.mushroom())) {
            platform.mario().contact(platform.mushroom());
        }

        if (CollisionHelper.isNearby(platform.mario(), platform.turtle())) {
            platform.mario().contact(platform.turtle());
        }
    }

    public void moveAll() {
        for (GameElement object : getObjects()) {
            object.move();
        }

        for (GameElement coin : platform.coins()) {
            coin.move();
        }

        platform.mushroom().move();
        platform.turtle().move();
    }

    public List<GameElement> getObjects() {
        return platform.objects();
    }

    public List<GameElement> getAllGameElements() {
        List<GameElement> list = new LinkedList<>(platform.objects());
        list.addAll(platform.coins());
        return list;
    }

    public Platform getPlatform() {
        return platform;
    }

    public Scene getScene() {
        return scene;
    }

    public void setScene(Scene scene) {
        this.scene = scene;
    }
}
